import React from "react";
import { useState } from "react";
import "./App.css";

const App = () => {
  const [player1DiceNumber, setPlayer1DiceNumber] = useState(1);
  const [player2DiceNumber, setPlayer2DiceNumber] = useState(2);
  let winner = "Player2 wins!";

  const findWinner = () => {
    let player1Dice = Math.floor(Math.random() * 6 + 1);
    let player2Dice = Math.floor(Math.random() * 6 + 1);
    setPlayer1DiceNumber(player1Dice);
    setPlayer2DiceNumber(player2Dice);
  };

  if (player1DiceNumber > player2DiceNumber) {
    winner = `Player-1 wins!`;
  } else if (player1DiceNumber < player2DiceNumber) {
    winner = `Player-2 wins!`;
  } else {
    winner = "Game tie";
  }

  return (
    <div className="bg-black h-screen text-orange-600 p-28 ">
      <div className="flex flex-col justify-center items-center">
        <h1>{winner}</h1>
        <div className="players flex m-8">
          <section className="player1 w-48 rotate-animation">
            <img src={`dice-${player1DiceNumber}.webp`} alt="player1dice" />
          </section>
          <section className="player2 w-48 rotate-animation">
            <img src={`dice-${player2DiceNumber}.webp`} alt="player2dice" />
          </section>
        </div>
        <button
          className="bg-orange-600 text-black px-3 rounded shadow-md"
          onClick={() => findWinner()}
        >
          play
        </button>
      </div>
    </div>
  );
};

export default App;
